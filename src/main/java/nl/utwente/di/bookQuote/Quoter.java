package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        if (isbn == "1"){
            return 10;
        } else if (isbn == "2") {
            return 45;
        }
        else if (isbn == "3"){
            return 20;
        } else if (isbn == "4") {
            return 35;
        } else if (isbn == "5") {
            return  50;
        }
        else {
            return 0;
        }

    }
}
